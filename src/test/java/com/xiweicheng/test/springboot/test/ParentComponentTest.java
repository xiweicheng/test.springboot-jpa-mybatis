package com.xiweicheng.test.springboot.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ParentComponentTest {

    @Autowired
    ParentComponent parentComponent;

    @Test
    public void save() {

        parentComponent.save("zhangsan", null);
    }

    @Test
    public void update() {

        parentComponent.update(6L, "wangwu1234561", 10);
    }

    @Test
    public void update2() {

        parentComponent.update2(6L, "zzz0000000", 10);
    }

    @Test
    public void remove() {
        parentComponent.remove(7L);
    }

    @Test
    public void get() {
        parentComponent.get(7L);
    }

    @Test
    @Transactional
    public void findUpdate() {
        parentComponent.findUpdate(7L, "aaa");
    }

}