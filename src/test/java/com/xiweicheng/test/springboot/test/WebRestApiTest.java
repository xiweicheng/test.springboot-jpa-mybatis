package com.xiweicheng.test.springboot.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class WebRestApiTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@org.junit.Test
	public void exampleTest() {
		String body = this.restTemplate.getForObject("/", String.class);
		assertThat(body).isEqualTo("spring boot test");
	}

	@org.junit.Test
	public void getTest() {
		String body = this.restTemplate.getForObject("/test/get/1", String.class);
		System.out.println(body);
//		assertThat(body).isEqualTo("spring boot test");
	}

}
