package com.xiweicheng.test.springboot.test;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(TestController.class)
public class TestControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	TestService testService;

	@Test
	public void testSave() throws Exception {

		given(this.testService.save("name")).willReturn(new com.xiweicheng.test.springboot.test.Test(1L, "name2"));
		this.mvc.perform(post("/test/save?name=name").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().json("{\"id\":1,\"name\":\"name2\"}"));
	}

	@Test
	public void testGet() throws Exception {

//		given(this.testService.get(1L)).willReturn(new com.xiweicheng.test.springboot.test.Test(1L, "name2"));

		this.mvc.perform(get("/test/get/1").accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().json("{\"id\":1,\"name\":\"name2\"}"));
	}

}
