package com.xiweicheng.test.springboot.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Example;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
//@Transactional(propagation = Propagation.NOT_SUPPORTED)
//@AutoConfigureTestDatabase(replace = Replace.NONE)
public class TestRepositoryTest {

	@Autowired
	TestEntityManager entityManager;

	@Autowired
	TestRepository testRepository;

	@Test
	public void test() {
		this.entityManager.persist(com.xiweicheng.test.springboot.test.Test.builder().name("lisi").build());

		com.xiweicheng.test.springboot.test.Test test = testRepository
				.findOne(Example.of(com.xiweicheng.test.springboot.test.Test.builder().name("lisi").build()));

		assertThat(test.getName()).isEqualTo("lisi");
	}

	@Test
	public void test2() {
		this.entityManager.persist(com.xiweicheng.test.springboot.test.Test.builder().name("lisi2").build());

		com.xiweicheng.test.springboot.test.Test test = testRepository
				.findOne(Example.of(com.xiweicheng.test.springboot.test.Test.builder().name("lisi2").build()));

		assertThat(test.getName()).isEqualTo("lisi2");
	}

}
