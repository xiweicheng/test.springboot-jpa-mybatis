package com.xiweicheng.test.springboot.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MockTest {

	@MockBean
	TestRepository testRepository;

	@Autowired
	TestService testService;

	@Test
	public void test() {
		given(testRepository.saveAndFlush(any()))
				.willReturn(com.xiweicheng.test.springboot.test.Test.builder().name("wangwu2").build());
		com.xiweicheng.test.springboot.test.Test test = testService.save("wangwu");
		assertThat(test.getName()).isEqualTo("wangwu2");
	}
}
