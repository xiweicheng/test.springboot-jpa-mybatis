package com.xiweicheng.test.springboot.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@org.springframework.boot.test.autoconfigure.json.JsonTest
public class JsonTest {

	@Autowired
	JacksonTester<Test> jacksonTester;

	@org.junit.Test
	public void testSerialize() throws Exception {

		Test test = new Test(1L, "zhaoliu");
		// Assert against a `.json` file in the same package as the test
		assertThat(this.jacksonTester.write(test)).isEqualToJson("test.json");
		// Or use JSON path based assertions
		assertThat(this.jacksonTester.write(test)).hasJsonPathNumberValue("@.id");
		assertThat(this.jacksonTester.write(test)).extractingJsonPathStringValue("@.name").isEqualTo("zhaoliu");
	}

	@org.junit.Test
	public void testDeserialize() throws Exception {

		String content = "{\"id\":1,\"name\":\"sunqi\"}";
		assertThat(this.jacksonTester.parse(content)).isEqualTo(new Test(1L, "sunqi"));
		assertThat(this.jacksonTester.parseObject(content).getName()).isEqualTo("sunqi");
	}
}
