package com.xiweicheng.test.springboot.test;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotBlank;

import com.google.gson.annotations.Expose;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

//@Cacheable(value = false)
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Test {

	@GeneratedValue
	@Id
	@Expose
	Long id;

	@NotBlank
	@Expose
	String name;

}
