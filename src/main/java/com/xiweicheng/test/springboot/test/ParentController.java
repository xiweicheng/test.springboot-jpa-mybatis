package com.xiweicheng.test.springboot.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("parent")
public class ParentController {

    @Autowired
    ParentComponent parentComponent;

    @GetMapping("get/{id}")
    public Parent get(@PathVariable("id") Long id) {

        return parentComponent.get(id);

    }

    @GetMapping("update/{id}")
    public Parent update(@PathVariable("id") Long id) {

        return parentComponent.findUpdate(id, "ddd");

    }

    @GetMapping("create/{name}")
    public Parent update(@PathVariable("name") String name) {

        return parentComponent.create(name, 100);

    }
}
