package com.xiweicheng.test.springboot.test;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface TestMapper {

    @Select("SELECT * FROM TEST WHERE id = #{id}")
    Test findById(@Param("id") Long id);
}
