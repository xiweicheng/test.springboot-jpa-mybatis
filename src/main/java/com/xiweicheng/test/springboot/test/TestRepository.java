package com.xiweicheng.test.springboot.test;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

public interface TestRepository extends JpaRepository<Test, Long> {

    @Transactional
    @Modifying
    @Query(value = "update test t set t.name=?1 where t.id=?2", nativeQuery = true)
    int updateNative(String name, Long id);

    @Transactional
    @Modifying
    @Query(value = "update Test t set t.name=?1 where t.id=?2", nativeQuery = false)
    int updateJpa(String name, Long id);

    @Query(value = "select t.* from test t where t.id=?1", nativeQuery = true)
    Test queryNative(Long id);

    @Query(value = "select t from Test t where t.id=?1", nativeQuery = false)
    Test queryJpa(Long id);
}
