package com.xiweicheng.test.springboot.test;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Component
@Slf4j
public class AsyncComponent {

    @Autowired
    ParentRepository parentRepository;

    @PersistenceContext
    EntityManager entityManager;

    @Async
    @Transactional
    public void get(Long id) {

        Parent parent = parentRepository.findOne(id);
        log.info("10:{}", parent);

        parent = parentRepository.findOne(id);
        log.info("20:{}", parent);

        parent = parentRepository.findById(id);
        log.info("30:{}", parent);

        parent = entityManager.find(Parent.class, id);
        log.info("40:{}", parent);
    }
}
