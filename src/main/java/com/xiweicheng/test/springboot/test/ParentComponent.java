package com.xiweicheng.test.springboot.test;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
@Slf4j
public class ParentComponent {

    @Autowired
    ParentRepository parentRepository;

    @Autowired
    ChildRepository childRepository;

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    AsyncComponent asyncComponent;

    @Autowired
    ParentComp parentComp;

    @Transactional
    public Parent create(String name, Integer age) {

        Parent parent = Parent.builder().name(name).age(age).build();

        Parent parent1 = parentRepository.saveAndFlush(parent);

        log.info("save parent");

        Child child1 = Child.builder().name(name + "-child1").age(age).parent(parent1).build();
        Child child2 = Child.builder().name(name + "-child2").age(age).parent(parent1).build();

        List<Child> childs = new ArrayList<>();
        childs.add(child1);
        childs.add(child2);

        childRepository.save(childs);
        childRepository.flush();

        log.info("save childs");

//        entityManager.detach(parent1);

        parent1.setChilds(childs);

//        parentRepository.saveAndFlush(parent1);

        log.info("save parent childs");

        return parent1;

    }

    public Parent save(String name, Integer age) {
        Parent parent = Parent.builder().name(name).age(age).build();

        Child child1 = Child.builder().name(name + "-child1").age(age).parent(parent).build();
        Child child2 = Child.builder().name(name + "-child2").age(age).parent(parent).build();

        List<Child> childs = new ArrayList<>();
        childs.add(child1);
        childs.add(child2);

        parent.setChilds(childs);

        Parent saved = parentRepository.saveAndFlush(parent);

        log.info("{}", saved);

        return saved;
    }

//    @Transactional
    public Parent update(Long id, String name, Integer age) {

        Parent parent = parentRepository.findOne(id);
        parent.setName(name);
        parent.setAge(age);

        for (Child child : parent.getChilds()) {
            child.setName(name);
            child.setAge(age);
        }

        childRepository.save(parent.getChilds());
        childRepository.flush();

        log.info("{}", parent);

        return parent;
    }

    @Transactional
    public Parent update2(Long id, String name, Integer age) {

        Parent parent = parentRepository.findOne(id);

        log.info("1:{}", parent);

        entityManager.detach(parent);
        parent.setName(name);
        parent.setAge(age);

        parentRepository.saveAndFlush(parent);

        log.info("2:{}", parent);

        return parent;
    }

//    @Transactional(rollbackOn = Exception.class)
    public void remove(Long id) {

        Parent parent = parentRepository.findOne(id);
        List<Child> childs = parent.getChilds();

        if(!CollectionUtils.isEmpty(childs)) {
            childs.remove(childs.toArray()[0]);
        }

        parentRepository.saveAndFlush(parent);
    }

    @Transactional
    public Parent get(Long id) {

        Parent parent = parentRepository.findOne(id);
        log.info("1:{}", parent);

        entityManager.clear();
        parent = parentRepository.findOne(id);
        log.info("2:{}", parent);

        entityManager.clear();
        parent = parentRepository.findById(id);
        log.info("3:{}", parent);

        entityManager.clear();
        parent = entityManager.find(Parent.class, id);
        log.info("4:{}", parent);

//        entityManager.clear();
        asyncComponent.get(id);

        parent = entityManager.find(Parent.class, id);
        log.info("5:{}", parent);

        return parent;
    }

//    @org.springframework.transaction.annotation.Transactional(propagation= Propagation.NEVER)
    public Parent findUpdate(Long id, String name) {

//        Parent parent = parentRepository.findOne(id);
        Parent parent = Parent.builder().id(id).build();

        Parent parent1 = entityManager.getReference(Parent.class, id);
        boolean contains = entityManager.contains(parent1);
        boolean c = parent == parent1;

        log.info("c1:{} {}", contains, c);
        log.info("1:{}", parent);

        entityManager.clear();

        contains = entityManager.contains(parent);
        log.info("c2:{}", contains);

        contains = entityManager.contains(Parent.builder().id(id).build());
        log.info("c3:{}", contains);

        Parent parent2 = parentRepository.findOne(id);
        parent2.setName(name);
        parentRepository.saveAndFlush(parent2);

        log.info("2:{}", parent2);
        log.info("3:{}", parent);

//        parentComp.refresh(parent);// java.lang.IllegalArgumentException: Entity not managed

        parentComp.refresh(parent2);

        // 需要包裹事务
//        entityManager.refresh(parent2);

        log.info("4:{}", parent);
        log.info("5:{}", parent2);

        return parent2;

    }

}
