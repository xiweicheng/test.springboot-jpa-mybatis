package com.xiweicheng.test.springboot.test;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Component
@Slf4j
public class ParentComp {

    @Autowired
    ParentRepository parentRepository;

    @Autowired
    ChildRepository childRepository;

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    AsyncComponent asyncComponent;

    @Transactional
    public Parent refresh(Parent parent) {

        log.info("10:{}", parent);

        entityManager.refresh(parent);

        log.info("20:{}", parent);

        return parent;

    }

}
