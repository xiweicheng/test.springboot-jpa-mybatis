package com.xiweicheng.test.springboot.test;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.Expose;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Data
@Builder
@ToString(exclude = {"parent"})
@NoArgsConstructor
@AllArgsConstructor
@DynamicInsert
@DynamicUpdate
@SelectBeforeUpdate
public class Child {

    @GeneratedValue
    @Id
    @Expose
    Long id;

    @Expose
    String name;

    @Expose
    @Column(nullable = true)
    Integer age;

//    @ManyToOne(cascade = CascadeType.ALL)
    @ManyToOne
    @JsonIgnore
    Parent parent;

}
