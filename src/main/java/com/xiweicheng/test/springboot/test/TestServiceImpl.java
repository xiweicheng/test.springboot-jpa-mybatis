package com.xiweicheng.test.springboot.test;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Service
@Slf4j
public class TestServiceImpl implements TestService {

    @Autowired
    TestRepository testRepository;

    @Autowired
    TestMapper testMapper;

    @Autowired
    EntityManager entityManager;

    @Override
    public Test save(String name) {

        return testRepository.saveAndFlush(Test.builder().name(name).build());
    }

    @Override
//	@Transactional
    public Test get(Long id) {

        Test test;

        testRepository.updateNative("zhangsan", 1L);

//		Test test = testRepository.queryNative(1L);

        test = testRepository.findOne(id);
        log.info("jpa first findOne: {}", test);

//		testRepository.updateNative("native", 1L);

        testRepository.updateJpa("jpa", 1L);

        test = testRepository.queryJpa(1L);
        log.info("jpa queryJpa: {}", test);

//        log.info("jpa detach test entity start... {}", test);
//        entityManager.detach(test);
//        log.info("jpa detach test entity end... {}", test);

//        log.info("jpa clear entity start... {}", test);
//        entityManager.clear();
//        log.info("jpa clear entity end... {}", test);

        test = testRepository.queryNative(1L);
        log.info("jpa queryNative: {}", test);

        test = testMapper.findById(1L);
        log.info("mybatis findById: {}", test);

        test = testRepository.findOne(id);
        log.info("jpa last findOne: {}", test);

        return test;
    }

}
