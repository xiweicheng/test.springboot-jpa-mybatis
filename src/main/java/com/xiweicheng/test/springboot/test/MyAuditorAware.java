package com.xiweicheng.test.springboot.test;

import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

@Component
public class MyAuditorAware implements AuditorAware<String> {

    @Override
    public String getCurrentAuditor() {
        return "tester";
    }
}
