package com.xiweicheng.test.springboot.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping()
public class TestController {

	@Autowired
	TestService testService;

	@PostMapping("test/save")
	public Test save(@RequestParam("name") String name) {

		Assert.notNull(name, "param name must not null!");

		return testService.save(name);
	}

	@GetMapping("test/get/{id}")
	public Test get(@PathVariable("id") Long id) {

		return testService.get(id);

	}

	@GetMapping("api/v1/test/echo")
	public String v1() {
		return "v1";
	}

	@GetMapping("api/v2/test/echo")
	public String v2() {
		return "v2";
	}
}
