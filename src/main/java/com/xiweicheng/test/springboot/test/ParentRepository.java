package com.xiweicheng.test.springboot.test;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ParentRepository extends JpaRepository<Parent, Long> {

    Parent findById(Long id);

}
